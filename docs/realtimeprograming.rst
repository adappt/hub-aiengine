Injecting code on the engine
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

One of the cool features of the engine is the ability to change the behavior while is executing.
The best way to understand this feature is by having a proper example.
We load the library and create a StackLan object with some memory requirements.

.. code:: python

  import pyaiengine

  s = pyaiengine.StackLan()

  s.tcp_flows = 32768
  s.udp_flows = 56384

Just for the example we are going to create 3 DNS rules for handling queries.

.. code:: python

  d1 = pyaiengine.DomainName("Generic net queries",".net")
  d2 = pyaiengine.DomainName("Generic com queries",".com")
  d3 = pyaiengine.DomainName("Generic org queries",".org")

  dm = pyaiengine.DomainManager()

  """ Add the DomainName objects to the manager """
  dm.add_domain_name(d1)
  dm.add_domain_name(d2)
  dm.add_domain_name(d3)

  st.set_domain_name_manager(dm,"DNSProtocol")

Now we open a new context of a PacketDispatcher and enable the shell for interacting with the engine.

.. code:: python

  with pyaiengine.PacketDispatcher("enp0s25") as pd:
      pd.stack = st
      """ We enable the shell for interact with the engine """
      pd.enable_shell = True
      pd.run()

If we execute this code we will see the following messages.

.. code:: bash

  [luis@localhost ai]$ python example.py
  [09/30/16 21:48:41] Lan network stack ready.
  AIEngine 1.6 shell
  [09/30/16 21:48:41] Processing packets from device enp0s25
  [09/30/16 21:48:41] Stack 'Lan network stack' using 51 MBytes of memory

  >>>

Now we are under control of the internal shell of the engine and we can access to the different
components.

.. code:: bash

  >>> print(dm)
  DomainNameManager (Generic Domain Name Manager)
          Name:Generic net queries      Domain:.net     Matchs:10
          Name:Generic org queries      Domain:.org     Matchs:0
          Name:Generic com queries      Domain:.com     Matchs:21

  >>>

And now we inject a callback function for one of the given domains.

.. code:: bash

  >>> def my_callback(flow):
  ...   d = flow.dns_info
  ...   if (d):
  ...     print(str(d))
  ...
  >>> d3.callback = my_callback
  >>>


And wait for domains that ends on .org

.. code:: bash

  >>>  Domain:www.gnu.org

also verify the rest of the components

.. code:: bash

  >>> print(d2)
  Name:Generic org queries      Domain:.org     Matchs:1        Callback:<function my_callback 0x023ffeea378>
  >>> print(dm)
  DomainNameManager (Generic Domain Name Manager)
          Name:Generic net queries      Domain:.net     Matchs:14
          Name:Generic org queries      Domain:.org     Matchs:1        Callback:<function my_callback 0x023ffeea378>
          Name:Generic com queries      Domain:.com     Matchs:21

Check the global status by executing the method show_protocol_statisitics

.. code:: bash

  >>> st.show_protocol_statistics()
  Protocol statistics summary
	Protocol       Bytes      Packets  % Bytes  CacheMiss  Memory      UseMemory    CacheMemory   Dynamic
	Ethernet       3030778    11681    100      0          192 Bytes   192 Bytes    0 Bytes       no
	VLan           0          0        0        0          192 Bytes   192 Bytes    0 Bytes       no
	MPLS           0          0        0        0          192 Bytes   192 Bytes    0 Bytes       no
	IP             2642875    9356     87       0          216 Bytes   216 Bytes    0 Bytes       no
	TCP            1388303    5224     45       210        9 KBytes    44 KBytes    0 Bytes       yes
	UDP            977364     4112     32       436        312 Bytes   116 KBytes   0 Bytes       yes
	ICMP           0          17       0        0          224 Bytes   224 Bytes    0 Bytes       no
	HTTP           0          0        0        0          800 Bytes   800 Bytes    0 Bytes       yes
	SSL            1012883    1779     33       0          12 KBytes   8 KBytes     1 KBytes      yes
	SMTP           0          0        0        0          440 Bytes   440 Bytes    0 Bytes       yes
	IMAP           0          0        0        0          376 Bytes   376 Bytes    0 Bytes       yes
	POP            0          0        0        0          376 Bytes   376 Bytes    0 Bytes       yes
	Bitcoin        0          0        0        0          240 Bytes   240 Bytes    0 Bytes       yes
	Modbus         0          0        0        0          232 Bytes   232 Bytes    0 Bytes       no
	MQTT           0          0        0        0          344 Bytes   344 Bytes    0 Bytes       yes
	TCPGeneric     173981     491      5        0          216 Bytes   216 Bytes    0 Bytes       no
	TCPFrequency   0          0        0        0          248 Bytes   248 Bytes    0 Bytes       yes
	DNS            174666     748      5        0          24 KBytes   20 KBytes    3 KBytes      yes
	SIP            0          0        0        0          576 Bytes   576 Bytes    0 Bytes       yes
	DHCP           21704      72       0        0          1 KBytes    1 KBytes     0 Bytes       yes
	NTP            0          0        0        0          224 Bytes   224 Bytes    0 Bytes       no
	SNMP           0          0        0        0          224 Bytes   224 Bytes    0 Bytes       no
	SSDP           1368       8        0        0          752 Bytes   752 Bytes    0 Bytes       yes
	Netbios        85897      1231     2        0          3 KBytes    2 KBytes     199 Bytes     yes
	CoAP           0          0        0        0          1 KBytes    1 KBytes     0 Bytes       yes
	RTP            0          0        0        0          216 Bytes   216 Bytes    0 Bytes       no
	Quic           558927     853      18       0          192 Bytes   192 Bytes    0 Bytes       no
	UDPGeneric     134802     764      4        0          216 Bytes   216 Bytes    0 Bytes       no
	UDPFrequency   0          0        0        0          248 Bytes   248 Bytes    0 Bytes       yes
	Total          3030778    11681    100      646        59 KBytes   203 KBytes   5 KBytes
