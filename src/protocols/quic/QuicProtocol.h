/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#ifndef SRC_PROTOCOLS_QUIC_QUICPROTOCOL_H_
#define SRC_PROTOCOLS_QUIC_QUICPROTOCOL_H_

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include "Protocol.h"
#include <arpa/inet.h>

namespace aiengine {

typedef struct {
	uint8_t flags;
	uint64_t cid;
	uint32_t version;
	uint8_t pkt_number;
	u_char data[0];
} __attribute__((packed)) quic_hdr;

class QuicProtocol: public Protocol {
public:
    	explicit QuicProtocol();
    	virtual ~QuicProtocol() {}

	static const uint16_t id = 0;	
	static constexpr int header_size = sizeof(quic_hdr);

	int getHeaderSize() const { return header_size;}

	int64_t getTotalBytes() const { return total_bytes_;}
	int64_t getTotalPackets() const { return total_packets_;}
	int64_t getTotalValidatedPackets() const { return total_validated_packets_;}
	int64_t getTotalMalformedPackets() const { return total_malformed_packets_;}

        void processFlow(Flow *flow);
        bool processPacket(Packet& packet) { return true; } 

	void setStatisticsLevel(int level) { stats_level_ = level;}
	void statistics(std::basic_ostream<char>& out);
	void statistics() { statistics(std::cout);}

	void releaseCache() {} // No need to free cache

	void setHeader(unsigned char *raw_packet){ 

		quic_header_ = reinterpret_cast <quic_hdr*> (raw_packet);
	}

	// Condition for say that a packet is quic
	bool quicChecker(Packet &packet){ 
	
		int length = packet.getLength();

		if (length >= header_size) {
			// Quic is started by the clients so just the destination port is checked 
			// and the first packet read must be one
			if ((packet.getDestinationPort() == 80)or 
				(packet.getDestinationPort() == 443)) {

				setHeader(packet.getPayload());
				if (quic_header_->pkt_number == 0x01) {
					++total_validated_packets_; 
					return true;
				}
			}
		}
		++total_malformed_packets_;
		return false;
	}

	int64_t getCurrentUseMemory() const { return sizeof(QuicProtocol); }
	int64_t getAllocatedMemory() const { return sizeof(QuicProtocol); }
	int64_t getTotalAllocatedMemory() const { return sizeof(QuicProtocol); }

        void setDynamicAllocatedMemory(bool value) {}
        bool isDynamicAllocatedMemory() const { return false; }

#if defined(PYTHON_BINDING)
        boost::python::dict getCounters() const;
#elif defined(RUBY_BINDING)
        VALUE getCounters() const;
#elif defined(JAVA_BINDING)
        JavaCounters getCounters() const  { JavaCounters counters; return counters; }
#elif defined(LUA_BINDING)
        LuaCounters getCounters() const;
#endif

private:
	int stats_level_;
	quic_hdr *quic_header_;
	int64_t total_bytes_;
};

typedef std::shared_ptr<QuicProtocol> QuicProtocolPtr;

} // namespace aiengine

#endif  // SRC_PROTOCOLS_QUIC_QUICPROTOCOL_H_
