/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "DNSInfo.h"

namespace aiengine {

void DNSInfo::reset() { 

	name.reset() ; 
	qtype_ = 0; 
	items_.clear(); 
	matched_domain_name.reset(); 
}

void DNSInfo::serialize(std::ostream& stream) {

        bool have_item = false;
#ifdef HAVE_FLOW_SERIALIZATION_COMPRESSION 
        stream << ",\"i\":{";
        if (name) {
                stream << "\"h\":\"" << name->getName() << "\"";
                have_item = true;
        }
        if (matched_domain_name) {
                if (have_item) stream << ",";
                stream << "\"m\":\"" << matched_domain_name->getName() << "\"";
        }
        if (have_item) stream << ",";
        stream << "\"t\":" << qtype_;

	if (items_.size() > 0 ) {
		stream << ",\"a\":\"";
		for (auto it = items_.begin(); it != items_.end(); ++it ) {
			stream << *it;
                	if ((it + 1) != items_.end()) stream << ",";
		}			
		stream << "\"";
	}
#else
        stream << ",\"info\":{";
        if (name) {
                stream << "\"dnsdomain\":\"" << name->getName() << "\"";
                have_item = true;
        }
        if (matched_domain_name) {
                if (have_item) stream << ",";
                stream << "\"matchs\":\"" << matched_domain_name->getName() << "\"";
        }
        if (have_item) stream << ",";
        stream << "\"qtype\":" << qtype_;

	if (items_.size() > 0 ) {
		stream << ",\"ips\":\"";
		for (auto it = items_.begin(); it != items_.end(); ++it ) {
			stream << *it;
			if ((it + 1) != items_.end()) stream << ",";
		}
		stream << "\"";
	}
#endif
        stream << "}";

}

void DNSInfo::addIPAddress(const char* ipstr) { 
	
	items_.emplace_back(ipstr); 
}

void DNSInfo::addName(const char* name) { 
	
	items_.emplace_back(name); 
}

} // namespace aiengine
