/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "test_netbios.h"

#define BOOST_TEST_DYN_LINK
#ifdef STAND_ALONE
#define BOOST_TEST_MODULE ntptest
#endif
#include <boost/test/unit_test.hpp>

using namespace aiengine;

BOOST_FIXTURE_TEST_SUITE(netbios_suite,StackNetbiostest)

BOOST_AUTO_TEST_CASE (test1_netbios)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_udp_netbios_137_1);
        int length = raw_packet_ethernet_ip_udp_netbios_137_1_length;
        Packet packet(pkt,length);

	inject(packet);

        BOOST_CHECK(ip->getTotalPackets() == 1);
        BOOST_CHECK(ip->getTotalValidatedPackets() == 1);
        BOOST_CHECK(ip->getTotalBytes() == 78);
        BOOST_CHECK(ip->getTotalMalformedPackets() == 0);

        BOOST_CHECK(netbios->getTotalPackets() == 1);
        BOOST_CHECK(netbios->getTotalValidatedPackets() == 1);
        BOOST_CHECK(netbios->getTotalBytes() == 50);
        BOOST_CHECK(netbios->getTotalMalformedPackets() == 0);

	Flow *flow = netbios->getCurrentFlow();
	BOOST_CHECK(flow != nullptr);
	SharedPointer<NetbiosInfo> info = flow->getNetbiosInfo();
	BOOST_CHECK(info != nullptr);

	std::string nbname("TEST");
	BOOST_CHECK(nbname.compare(info->netbios_name->getName()) == 0);
	BOOST_CHECK(netbios->getTotalEvents() == 0);
}

BOOST_AUTO_TEST_CASE (test2_netbios)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_udp_netbios_137_2);
        int length = raw_packet_ethernet_ip_udp_netbios_137_2_length;
        Packet packet(pkt,length);

	inject(packet);

        BOOST_CHECK(ip->getTotalPackets() == 1);
        BOOST_CHECK(ip->getTotalValidatedPackets() == 1);
        BOOST_CHECK(ip->getTotalBytes() == 78);
        BOOST_CHECK(ip->getTotalMalformedPackets() == 0);

        BOOST_CHECK(netbios->getTotalPackets() == 1);
        BOOST_CHECK(netbios->getTotalValidatedPackets() == 1);
        BOOST_CHECK(netbios->getTotalBytes() == 50);
        BOOST_CHECK(netbios->getTotalMalformedPackets() == 0);
	
	Flow *flow = netbios->getCurrentFlow();
	BOOST_CHECK(flow != nullptr);
	SharedPointer<NetbiosInfo> info = flow->getNetbiosInfo();
	BOOST_CHECK(info != nullptr);

	std::string nbname("ISATAP");
	BOOST_CHECK(nbname.compare(info->netbios_name->getName()) == 0);
}

BOOST_AUTO_TEST_CASE (test3_netbios)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_udp_netbios_137_3);
        int length = raw_packet_ethernet_ip_udp_netbios_137_3_length;
        Packet packet(pkt,length);

        inject(packet);

        BOOST_CHECK(ip->getTotalPackets() == 1);
        BOOST_CHECK(ip->getTotalValidatedPackets() == 1);
        BOOST_CHECK(ip->getTotalBytes() == 78);
        BOOST_CHECK(ip->getTotalMalformedPackets() == 0);

        BOOST_CHECK(netbios->getTotalPackets() == 1);
        BOOST_CHECK(netbios->getTotalValidatedPackets() == 1);
        BOOST_CHECK(netbios->getTotalBytes() == 50);
        BOOST_CHECK(netbios->getTotalMalformedPackets() == 0);

        Flow *flow = netbios->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<NetbiosInfo> info = flow->getNetbiosInfo();
        BOOST_CHECK(info != nullptr);

        std::string nbname("58CLV4J");
        BOOST_CHECK(nbname.compare(info->netbios_name->getName()) == 0);
}

// Verifying the anomaly on the netbios side
BOOST_AUTO_TEST_CASE (test4_netbios)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_udp_netbios_137_3);
        int length = raw_packet_ethernet_ip_udp_netbios_137_3_length;
        Packet packet(pkt,length - 8);

        inject(packet);

        BOOST_CHECK(ip->getTotalPackets() == 1);
        BOOST_CHECK(ip->getTotalValidatedPackets() == 1);
        BOOST_CHECK(ip->getTotalBytes() == 70);
        BOOST_CHECK(ip->getTotalMalformedPackets() == 0);

        BOOST_CHECK(netbios->getTotalPackets() == 1);
        BOOST_CHECK(netbios->getTotalValidatedPackets() == 1);
        BOOST_CHECK(netbios->getTotalBytes() == 42);
        BOOST_CHECK(netbios->getTotalMalformedPackets() == 0);

        Flow *flow = netbios->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<NetbiosInfo> info = flow->getNetbiosInfo();
        BOOST_CHECK(info != nullptr);

        PacketAnomalyType pa = flow->getPacketAnomaly();
        BOOST_CHECK(pa == PacketAnomalyType::NETBIOS_BOGUS_HEADER);
	BOOST_CHECK(netbios->getTotalEvents() == 1);
}

BOOST_AUTO_TEST_CASE (test5_netbios)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_udp_netbios_137_4);
        int length = raw_packet_ethernet_ip_udp_netbios_137_4_length;
        Packet packet(pkt,length);

        inject(packet);

        BOOST_CHECK(ip->getTotalPackets() == 1);
        BOOST_CHECK(ip->getTotalValidatedPackets() == 1);
        BOOST_CHECK(ip->getTotalBytes() == 78);
        BOOST_CHECK(ip->getTotalMalformedPackets() == 0);

        BOOST_CHECK(netbios->getTotalPackets() == 1);
        BOOST_CHECK(netbios->getTotalValidatedPackets() == 1);
        BOOST_CHECK(netbios->getTotalBytes() == 50);
        BOOST_CHECK(netbios->getTotalMalformedPackets() == 0);

        Flow *flow = netbios->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<NetbiosInfo> info = flow->getNetbiosInfo();
        BOOST_CHECK(info != nullptr);

	// std::cout << info->netbios_name->getName() << std::endl;
        std::string nbname("NAMESERVER.UM");
        BOOST_CHECK(nbname.compare(info->netbios_name->getName()) == 0);
}

BOOST_AUTO_TEST_CASE (test6_netbios)
{
        unsigned char *pkt = reinterpret_cast <unsigned char*> (raw_packet_ethernet_ip_udp_netbios_137_5);
        int length = raw_packet_ethernet_ip_udp_netbios_137_5_length;
        Packet packet(pkt,length);

        inject(packet);

        BOOST_CHECK(ip->getTotalPackets() == 1);
        BOOST_CHECK(ip->getTotalValidatedPackets() == 1);
        BOOST_CHECK(ip->getTotalBytes() == 78);
        BOOST_CHECK(ip->getTotalMalformedPackets() == 0);

        BOOST_CHECK(netbios->getTotalPackets() == 1);
        BOOST_CHECK(netbios->getTotalValidatedPackets() == 1);
        BOOST_CHECK(netbios->getTotalBytes() == 50);
        BOOST_CHECK(netbios->getTotalMalformedPackets() == 0);

        Flow *flow = netbios->getCurrentFlow();
        BOOST_CHECK(flow != nullptr);
        SharedPointer<NetbiosInfo> info = flow->getNetbiosInfo();
        BOOST_CHECK(info != nullptr);

        // std::cout << info->netbios_name->getName() << std::endl;
        std::string nbname("__MSBROWSE__");
        BOOST_CHECK(nbname.compare(info->netbios_name->getName()) == 0);
}

BOOST_AUTO_TEST_SUITE_END()

