/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include "Packet.h"

namespace aiengine {

Packet::Packet(unsigned char *packet,int length, int prev_header_size,
	PacketAnomalyType pa, time_t packet_time):
	curr_packet(packet,length),
	prev_packet(packet,length),
	link_packet(packet,length),
	net_packet(packet,length),
	trans_packet(packet,length),
	prev_header_size_(prev_header_size),
	source_port_(0),
	dest_port_(0),
	pa_(pa),
	packet_time_(packet_time),
	have_tag_(false),
	have_evidence_(false),
	force_adaptor_write_(false),
#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(JAVA_BINDING) || defined(LUA_BINDING)
        is_accept_(true),
#endif
	tag_(0xffffffff) 
	{}
	
Packet::Packet(const Packet& p):
	curr_packet(p.curr_packet),
	prev_packet(p.prev_packet),
	link_packet(p.link_packet),
	net_packet(p.net_packet),
	trans_packet(p.trans_packet),
	prev_header_size_(p.prev_header_size_),
	source_port_(p.source_port_),
	dest_port_(p.dest_port_),
	pa_(p.pa_),
	packet_time_(p.packet_time_),
	have_tag_(p.have_tag_),
	have_evidence_(p.have_evidence_),
	force_adaptor_write_(p.force_adaptor_write_),
#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(JAVA_BINDING) || defined(LUA_BINDING)
        is_accept_(p.is_accept_),
#endif
	tag_(p.tag_) 
	{}

void Packet::setTag(uint32_t tag) { 

	have_tag_ = true; 
	tag_ = tag; 
}

void Packet::setPayload(unsigned char *packet) { 

	prev_packet.setPayload(curr_packet.getPayload()); 
	curr_packet.setPayload(packet); 
}

/* LCOV_EXCL_START */
std::ostream& operator<<(std::ostream& os, const Packet& p) {
	
	os << "Begin packet(" << &p << ") length:" << p.curr_packet.getLength() << " prev header size:" << p.prev_header_size_;
	os << " anomaly:" << " " /* PacketAnomalies[static_cast<int8_t>(p.pa_)].name */ << " time:" << p.packet_time_;
	os << " sport:" << p.source_port_ << " dport:" << p.dest_port_ << " evi:" << p.have_evidence_ << std::endl;

	showPayload(os,p.curr_packet.getPayload(),p.curr_packet.getLength());

	return os;
}	
/* LCOV_EXCL_STOP */

} // namespace aiengine

