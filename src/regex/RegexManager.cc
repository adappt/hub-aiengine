/*
 * AIEngine a new generation network intrusion detection system.
 *
 * Copyright (C) 2013-2017  Luis Campo Giralte
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 *
 * Written by Luis Campo Giralte <luis.camp0.2009@gmail.com> 
 *
 */
#include <iostream>
#include "RegexManager.h"

namespace aiengine {

RegexManager::RegexManager(const std::string& name):
#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(JAVA_BINDING) || defined(LUA_BINDING)
        call(),
#endif
	name_(name),
	plugged_to_name_(""),
	current_signature_(),
        total_matched_signatures_(0),
        signatures_() 
	{}

#if defined(PYTHON_BINDING) 
RegexManager::RegexManager(const std::string& name, boost::python::list& regexs):
	RegexManager(name) {

	for (int i = 0; i < len(regexs); ++i ) {
                // Check if is a SharedPointer<Regex>
		boost::python::extract<SharedPointer<Regex>> extractor(regexs[i]);
		if (extractor.check()) {
			auto re = extractor();
			
			addRegex(re);
		}
	}
}
#endif

void RegexManager::removeRegex(const std::string& name,const std::string& expression) {

	for (auto it = signatures_.begin(); it != signatures_.end(); ) {
                auto ssig = (*it);

		if ((name.compare(ssig->getName()) == 0)and(expression.compare(ssig->getExpression()) == 0)) {
			// Erase the item
			it = signatures_.erase(it);
		} else {
			++it;	
		}
	}
}

void RegexManager::removeRegex(const SharedPointer<Regex>& sig) {

	for (auto it = signatures_.begin(); it != signatures_.end(); ) {
                auto ssig = (*it);

		if (ssig == sig) {
			// Erase the item
			it = signatures_.erase(it);
		} else {
			++it;
		}
	}
}

void RegexManager::addRegex(const std::string& name,const std::string& expression) {

        SharedPointer<Regex> sig = SharedPointer<Regex>(new Regex(name,expression));

        addRegex(sig);
}

void RegexManager::addRegex(const SharedPointer<Regex>& sig) {

        signatures_.push_back(sig);
}

void RegexManager::evaluate(boost::string_ref &data, bool *result) {

	current_signature_.reset();

        for (auto &sig: signatures_) {

                if (sig->evaluate(data)) {
                        ++total_matched_signatures_;
                        current_signature_ = sig;
                        (*result) = true;
                        break;
                }
        }
        return;
}

void RegexManager::show_regex(std::basic_ostream<char>& out,std::function<bool (const Regex&)> condition) const {

        out << "RegexManager(" << std::addressof(*this) << ")[" << name_ << "]" << std::dec;

        if (plugged_to_name_.length() > 0) {
                out << " Plugged on " << plugged_to_name_;
        }

#if defined(PYTHON_BINDING) || defined(RUBY_BINDING) || defined(JAVA_BINDING) || defined(LUA_BINDING)
	if (call.haveCallback()) out << " Callback:" << call.getCallbackName(); 
#endif
	out << std::endl;

        for (auto &it : signatures_ ) {
                SharedPointer<Regex> ssig = it;
                std::ostringstream tabs;

                bool no_more_regex = false;

                while (no_more_regex == false) {
                        tabs << "\t";

			if (condition(*ssig.get())) {
        			out << tabs.str() <<  *ssig.get();
                        }
                        if (ssig->isTerminal() == false) {

                                if (ssig->getNextRegexManager())
                                        break;

                                no_more_regex = false;
                                SharedPointer<Regex> raux = ssig->getNextRegex();
                                if (raux)
                                        ssig = raux;
                        } else {
                                no_more_regex = true;
                        }
                }
        }
}

void RegexManager::statistics() const { 

	statistics(std::cout);
}

void RegexManager::statistics(std::basic_ostream<char>& out) const {

	show_regex(out, [&] (const Regex& f) { return true; });
}

void RegexManager::statistics(const std::string &name ) const {

	show_regex(std::cout, [&] (const Regex& f) 
	{ 
		if (name.compare(f.getName()) == 0) 
			return true;
		else
			return false; 
	});
}

std::ostream& operator<< (std::ostream& out, const RegexManager& sig) {

	sig.statistics(out);
	return out;
}

} // namespace aiengine
